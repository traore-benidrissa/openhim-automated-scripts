#!/bin/bash
#Install OpenHIM-Core and OpenHIM-Console
#On ubuntu 18.0.4 LTS / Server / Desktop
#for test/development purpose
#not yet for production
#from install steps on https://openhim.readthedocs.io/en/latest/getting-started.html
#sample script https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh
#authors: Ben Idrissa, WAHIT
#set log file
#set variable
#set coreIP="192.168.100.100"
#set consIP= "192.168.100.100"
set releases="v1.12.0.tar" # openhimconsole release
I_CORE=0
I_CONS=1
IP_CORE="192.168.100.100"
IP_CONS="192.168.100.100"
CURR_PWD=$PWD
#
#
#
#
#
#
#
#
#function install core
function installCore()
{
    updateSys
    #create install dir in ~/tmp/openhim-install
    #good to get current PWD and restore it at the end
    currentPWD="$(pwd)"

    sudo mkdir -p ~/tmp/openhim-install && sudo cd ~/tmp/openhim-install
    #
    #
    echo "########################## installing nodejs"
    sudo apt install curl
    sleep 5
    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash && sudo apt install -y nodejs
    #
    echo "########################## installing mongodb"
    sleep 5
    sudo apt install -y mongodb 
    #
    echo "########################## configure MongoDB as a systemctl service that will automatically start on boot"
    sudo systemctl start mongodb
    sudo systemctl enable mongodb
    sudo systemctl status mongodb
    #
    #if restoreTestData
    #call function restore-testdata-in-mongodb
    #end if
    echo "########################## Install Git "
    sudo apt install -y git
    echo "########################## Install npm "
    sleep 5
    sudo apt install -y npm
    echo "########################## Install the OpenHIM-core package globally"
    echo "########################## (this will also install an OpenHIM-core binary to your PATH)"
    echo "########################## install only when not installed or version lt than 4 availlable"
    OHCOREVERSION=$(npm list -g openhim-core | grep openhim-core@ | sed 's/.*openhim-core@//g' | sed 's/\..*//g')
    #to avoid "checkPermissions Missing write access to /usr/lib/node_modules" error during npm install 
    ([ -z "$OHCOREVERSION" ] || [ "$OHCOREVERSION" -lt 4  ]) && npm install openhim-core -g 
    echo "########################## openhimcore installed: $(npm list -g openhim-core | grep openhim-core@)"
    echo "########################## getting the default settings:"
    rm default.json
    wget https://raw.githubusercontent.com/jembi/openhim-core-js/master/config/default.json
    #createll /etc/openhim/
    sleep 5
    sudo mkdir -p /etc/openhim
    sudo mv default.json /etc/openhim
    #replace default ip with coreIP
    #copy in /etc/openhim/custom.json
    echo "########################## CORE IP ADDR: $IP_CORE"
    cat /etc/openhim/default.json | sed "s/\"bindAddress\":.*,/\"binAddress\": \"$IP_CORE\",/g" | sed "s/\"externalHostname\":.*,/\"externalHostname\": \"$IP_CORE\",/g" |sudo tee /etc/openhim/custom.json

    echo "########################## Start the server"
    #if installCoreAsService 
    #call function create-and-install-openhim-core-as-systemd-service
    #end if
    #if installCoreAsService = 0
        openhim-core --conf=/etc/openhim/custom.json
    #end if
    #end uu is ok
    #uu no ok 
    #call exit function with error message 
    #end uu no ok
}
#end function 

installConsole () {
#function install console 
echo "########################## Installing the OpenHIM Console"
echo "########################## Ensure the system update"
sudo apt update && sudo apt upgrade

echo "########################## install apache2"
sudo apt install apache2 



echo "########################## create app folder /var/www/openhimconsole"
sudo mkdir /var/www/openhimconsole
#
echo "########################## downloading the tarball"
#take link from https://github.com/jembi/openhim-console/releases/latest
wget https://github.com/jembi/openhim-console/releases/download/v1.13.0/openhim-console-v1.13.0.tar.gz
echo "########################## Export the contents"
CONSOLE="openhim-console-v1.13.0.tar.gz"
sudo rm $CONSOLE
sudo tar -vxzf $CONSOLE --directory /var/www/openhimconsole
#copy default config file to /var/www/openhimconsole/config/default.json.bak 
#create new config file with right version, ip
#get virtualhost template
sudo rm openhimconsole.conf
wget https://gitlab.com/traore-benidrissa/openhim-automated-scripts/raw/master/openhimconsole-tmp.conf -O openhimconsole.conf

cat openhimconsole.conf | sed "s/<VirtualHost \*:80>/<VirtualHost $IP_CONS:80>/g" |sudo tee openhimconsole.conf
#copy in site-availlable 
sudo mv openhimconsole.conf /etc/apache2/sites-available/
#enable new virtualHost in apache for openhimconsole
sudo a2ensite openhimconsole.conf

echo "########################## Start core
openhim-core --conf=/etc/openhim/custom.json


echo "########################## display apache2 status"
sudo systemctl reload apache2 || sudo systemctl start apache2 
sudo systemctl status apache2

#default access
#Username: root@openhim.org
#Password: openhim-password
#end func
}

#function create-and-install-openhim-core-as-systemd-service
#end func

#function disable or config firewall
#end func

#function setup exec env
#set log file
#set variable
#end

#function restore-testdata-in-mongodb
#restore or add test data in mongodb
#end

function updateSys()
{
#Ensure the system update to avaid 
#libappstream3 problems with the 
#npm package manager 
#
sudo apt update && sudo apt upgrade
#
#uu is ok
}

clear
#Ensure that the host is ubuntu 18.0.4 LTS, or 16.0.5 LTS
UBUNTU_VER=$(cat /etc/lsb-release | grep -E 'Ubuntu\ [0-9]*\.[0-9]*\.[0-9]\ LTS'|sed -e 's/[a-zA-Z]*\_[a-zA-Z]*\="Ubuntu\ //g' | sed -e 's/\.[0-9]*\.[0-9]*\ [a-zA-Z]*"//g') || "test"
#
#os is ok
if [ "$UBUNTU_VER" -ge 16 ]; then
echo "########################## Ubuntu version $UBUNTU_VER LTS"
echo "########################## Install core ? $I_CORE"
    #installCore
    if [  $I_CORE -eq 1 ]; then
        #call function setup exec env
        #call function install core
        installCore
    fi
#
#
    if [  $I_CONS -eq 1 ]; then
        #if installConsole
        #call function setup exec env
        #call function install console 
        installConsole
        echo ""
    fi
#end if
else #os no ok
#call exit function with error message 
echo "########################## script tested only for Ubuntu 16 LTS and +"
YOUR_SYS=$(cat /etc/lsb-release | grep -E 'Ubuntu\ [0-9]*\.[0-9]*\.[0-9]\ LTS'|sed -e 's/[a-zA-Z]*\_[a-zA-Z]*\="//g')
echo "########################## Your system: $YOUR_SYS"
#exit 200
fi
#end os nook